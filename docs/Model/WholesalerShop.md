# # WholesalerShop

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] [readonly] 
**name** | **string** |  | [optional] 
**site** | **string[]** |  | [optional] 
**wholesaler** | **string** |  | [optional] 
**site_url** | **string[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


