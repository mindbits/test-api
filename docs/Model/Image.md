# # Image

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image_file** | [**\OpenAPI\Client\Model\File**](File.md) |  | [optional] 
**image** | [**\OpenAPI\Client\Model\File**](File.md) |  | [optional] 
**product** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


