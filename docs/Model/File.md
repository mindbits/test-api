# # File

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mime_type** | **string** | Returns the mime type of the file. | [optional] [readonly] 
**path** | **string** |  | [optional] 
**filename** | [**object**](.md) |  | [optional] [readonly] 
**extension** | [**object**](.md) |  | [optional] [readonly] 
**basename** | [**object**](.md) |  | [optional] [readonly] 
**pathname** | [**object**](.md) |  | [optional] [readonly] 
**perms** | [**object**](.md) |  | [optional] [readonly] 
**inode** | [**object**](.md) |  | [optional] [readonly] 
**size** | [**object**](.md) |  | [optional] [readonly] 
**owner** | [**object**](.md) |  | [optional] [readonly] 
**group** | [**object**](.md) |  | [optional] [readonly] 
**a_time** | [**object**](.md) |  | [optional] [readonly] 
**m_time** | [**object**](.md) |  | [optional] [readonly] 
**c_time** | [**object**](.md) |  | [optional] [readonly] 
**type** | [**object**](.md) |  | [optional] [readonly] 
**writable** | **bool** |  | [optional] [readonly] 
**readable** | **bool** |  | [optional] [readonly] 
**executable** | **bool** |  | [optional] [readonly] 
**file** | **bool** |  | [optional] [readonly] 
**dir** | **bool** |  | [optional] [readonly] 
**link** | **bool** |  | [optional] [readonly] 
**link_target** | [**object**](.md) |  | [optional] [readonly] 
**real_path** | [**object**](.md) |  | [optional] [readonly] 
**file_info** | [**object**](.md) |  | [optional] [readonly] 
**path_info** | [**object**](.md) |  | [optional] [readonly] 
**file_class** | [**object**](.md) |  | [optional] 
**info_class** | [**object**](.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


