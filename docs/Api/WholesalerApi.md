# OpenAPI\Client\WholesalerApi

All URIs are relative to *https://ve-s1.jamm.media*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteWholesalerItem**](WholesalerApi.md#deleteWholesalerItem) | **DELETE** /api/wholesalers/{id} | Removes the Wholesaler resource.
[**getWholesalerCollection**](WholesalerApi.md#getWholesalerCollection) | **GET** /api/wholesalers | Retrieves the collection of Wholesaler resources.
[**getWholesalerItem**](WholesalerApi.md#getWholesalerItem) | **GET** /api/wholesalers/{id} | Retrieves a Wholesaler resource.
[**patchWholesalerItem**](WholesalerApi.md#patchWholesalerItem) | **PATCH** /api/wholesalers/{id} | Updates the Wholesaler resource.
[**postWholesalerCollection**](WholesalerApi.md#postWholesalerCollection) | **POST** /api/wholesalers | Creates a Wholesaler resource.
[**putWholesalerItem**](WholesalerApi.md#putWholesalerItem) | **PUT** /api/wholesalers/{id} | Replaces the Wholesaler resource.



## deleteWholesalerItem

> deleteWholesalerItem($id)

Removes the Wholesaler resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\WholesalerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 

try {
    $apiInstance->deleteWholesalerItem($id);
} catch (Exception $e) {
    echo 'Exception when calling WholesalerApi->deleteWholesalerItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getWholesalerCollection

> \OpenAPI\Client\Model\Wholesaler[] getWholesalerCollection($page)

Retrieves the collection of Wholesaler resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\WholesalerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$page = 56; // int | The collection page number

try {
    $result = $apiInstance->getWholesalerCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WholesalerApi->getWholesalerCollection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional]

### Return type

[**\OpenAPI\Client\Model\Wholesaler[]**](../Model/Wholesaler.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getWholesalerItem

> \OpenAPI\Client\Model\Wholesaler getWholesalerItem($id)

Retrieves a Wholesaler resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\WholesalerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 

try {
    $result = $apiInstance->getWholesalerItem($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WholesalerApi->getWholesalerItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\OpenAPI\Client\Model\Wholesaler**](../Model/Wholesaler.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchWholesalerItem

> \OpenAPI\Client\Model\Wholesaler patchWholesalerItem($id, $wholesaler)

Updates the Wholesaler resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\WholesalerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 
$wholesaler = new \OpenAPI\Client\Model\Wholesaler(); // \OpenAPI\Client\Model\Wholesaler | The updated Wholesaler resource

try {
    $result = $apiInstance->patchWholesalerItem($id, $wholesaler);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WholesalerApi->patchWholesalerItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **wholesaler** | [**\OpenAPI\Client\Model\Wholesaler**](../Model/Wholesaler.md)| The updated Wholesaler resource | [optional]

### Return type

[**\OpenAPI\Client\Model\Wholesaler**](../Model/Wholesaler.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/merge-patch+json
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## postWholesalerCollection

> \OpenAPI\Client\Model\Wholesaler postWholesalerCollection($wholesaler)

Creates a Wholesaler resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\WholesalerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$wholesaler = new \OpenAPI\Client\Model\Wholesaler(); // \OpenAPI\Client\Model\Wholesaler | The new Wholesaler resource

try {
    $result = $apiInstance->postWholesalerCollection($wholesaler);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WholesalerApi->postWholesalerCollection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wholesaler** | [**\OpenAPI\Client\Model\Wholesaler**](../Model/Wholesaler.md)| The new Wholesaler resource | [optional]

### Return type

[**\OpenAPI\Client\Model\Wholesaler**](../Model/Wholesaler.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/ld+json, application/json, text/html
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## putWholesalerItem

> \OpenAPI\Client\Model\Wholesaler putWholesalerItem($id, $wholesaler)

Replaces the Wholesaler resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\WholesalerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 
$wholesaler = new \OpenAPI\Client\Model\Wholesaler(); // \OpenAPI\Client\Model\Wholesaler | The updated Wholesaler resource

try {
    $result = $apiInstance->putWholesalerItem($id, $wholesaler);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WholesalerApi->putWholesalerItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **wholesaler** | [**\OpenAPI\Client\Model\Wholesaler**](../Model/Wholesaler.md)| The updated Wholesaler resource | [optional]

### Return type

[**\OpenAPI\Client\Model\Wholesaler**](../Model/Wholesaler.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/ld+json, application/json, text/html
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

