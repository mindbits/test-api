# OpenAPI\Client\SiteApi

All URIs are relative to *https://ve-s1.jamm.media*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteSiteItem**](SiteApi.md#deleteSiteItem) | **DELETE** /api/sites/{id} | Removes the Site resource.
[**getSiteCollection**](SiteApi.md#getSiteCollection) | **GET** /api/sites | Retrieves the collection of Site resources.
[**getSiteItem**](SiteApi.md#getSiteItem) | **GET** /api/sites/{id} | Retrieves a Site resource.
[**patchSiteItem**](SiteApi.md#patchSiteItem) | **PATCH** /api/sites/{id} | Updates the Site resource.
[**postSiteCollection**](SiteApi.md#postSiteCollection) | **POST** /api/sites | Creates a Site resource.
[**putSiteItem**](SiteApi.md#putSiteItem) | **PUT** /api/sites/{id} | Replaces the Site resource.



## deleteSiteItem

> deleteSiteItem($id)

Removes the Site resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SiteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 

try {
    $apiInstance->deleteSiteItem($id);
} catch (Exception $e) {
    echo 'Exception when calling SiteApi->deleteSiteItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getSiteCollection

> \OpenAPI\Client\Model\Site[] getSiteCollection($page)

Retrieves the collection of Site resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SiteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$page = 56; // int | The collection page number

try {
    $result = $apiInstance->getSiteCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SiteApi->getSiteCollection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional]

### Return type

[**\OpenAPI\Client\Model\Site[]**](../Model/Site.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getSiteItem

> \OpenAPI\Client\Model\Site getSiteItem($id)

Retrieves a Site resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SiteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 

try {
    $result = $apiInstance->getSiteItem($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SiteApi->getSiteItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\OpenAPI\Client\Model\Site**](../Model/Site.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchSiteItem

> \OpenAPI\Client\Model\Site patchSiteItem($id, $site)

Updates the Site resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SiteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 
$site = new \OpenAPI\Client\Model\Site(); // \OpenAPI\Client\Model\Site | The updated Site resource

try {
    $result = $apiInstance->patchSiteItem($id, $site);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SiteApi->patchSiteItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **site** | [**\OpenAPI\Client\Model\Site**](../Model/Site.md)| The updated Site resource | [optional]

### Return type

[**\OpenAPI\Client\Model\Site**](../Model/Site.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/merge-patch+json
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## postSiteCollection

> \OpenAPI\Client\Model\Site postSiteCollection($site)

Creates a Site resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SiteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$site = new \OpenAPI\Client\Model\Site(); // \OpenAPI\Client\Model\Site | The new Site resource

try {
    $result = $apiInstance->postSiteCollection($site);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SiteApi->postSiteCollection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **site** | [**\OpenAPI\Client\Model\Site**](../Model/Site.md)| The new Site resource | [optional]

### Return type

[**\OpenAPI\Client\Model\Site**](../Model/Site.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/ld+json, application/json, text/html
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## putSiteItem

> \OpenAPI\Client\Model\Site putSiteItem($id, $site)

Replaces the Site resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SiteApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 
$site = new \OpenAPI\Client\Model\Site(); // \OpenAPI\Client\Model\Site | The updated Site resource

try {
    $result = $apiInstance->putSiteItem($id, $site);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SiteApi->putSiteItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **site** | [**\OpenAPI\Client\Model\Site**](../Model/Site.md)| The updated Site resource | [optional]

### Return type

[**\OpenAPI\Client\Model\Site**](../Model/Site.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/ld+json, application/json, text/html
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

