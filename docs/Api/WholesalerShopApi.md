# OpenAPI\Client\WholesalerShopApi

All URIs are relative to *https://ve-s1.jamm.media*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteWholesalerShopItem**](WholesalerShopApi.md#deleteWholesalerShopItem) | **DELETE** /api/wholesaler_shops/{id} | Removes the WholesalerShop resource.
[**getWholesalerShopCollection**](WholesalerShopApi.md#getWholesalerShopCollection) | **GET** /api/wholesaler_shops | Retrieves the collection of WholesalerShop resources.
[**getWholesalerShopItem**](WholesalerShopApi.md#getWholesalerShopItem) | **GET** /api/wholesaler_shops/{id} | Retrieves a WholesalerShop resource.
[**patchWholesalerShopItem**](WholesalerShopApi.md#patchWholesalerShopItem) | **PATCH** /api/wholesaler_shops/{id} | Updates the WholesalerShop resource.
[**postWholesalerShopCollection**](WholesalerShopApi.md#postWholesalerShopCollection) | **POST** /api/wholesaler_shops | Creates a WholesalerShop resource.
[**putWholesalerShopItem**](WholesalerShopApi.md#putWholesalerShopItem) | **PUT** /api/wholesaler_shops/{id} | Replaces the WholesalerShop resource.



## deleteWholesalerShopItem

> deleteWholesalerShopItem($id)

Removes the WholesalerShop resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\WholesalerShopApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 

try {
    $apiInstance->deleteWholesalerShopItem($id);
} catch (Exception $e) {
    echo 'Exception when calling WholesalerShopApi->deleteWholesalerShopItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getWholesalerShopCollection

> \OpenAPI\Client\Model\WholesalerShop[] getWholesalerShopCollection($page)

Retrieves the collection of WholesalerShop resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\WholesalerShopApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$page = 56; // int | The collection page number

try {
    $result = $apiInstance->getWholesalerShopCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WholesalerShopApi->getWholesalerShopCollection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional]

### Return type

[**\OpenAPI\Client\Model\WholesalerShop[]**](../Model/WholesalerShop.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getWholesalerShopItem

> \OpenAPI\Client\Model\WholesalerShop getWholesalerShopItem($id)

Retrieves a WholesalerShop resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\WholesalerShopApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 

try {
    $result = $apiInstance->getWholesalerShopItem($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WholesalerShopApi->getWholesalerShopItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\OpenAPI\Client\Model\WholesalerShop**](../Model/WholesalerShop.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchWholesalerShopItem

> \OpenAPI\Client\Model\WholesalerShop patchWholesalerShopItem($id, $wholesaler_shop)

Updates the WholesalerShop resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\WholesalerShopApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 
$wholesaler_shop = new \OpenAPI\Client\Model\WholesalerShop(); // \OpenAPI\Client\Model\WholesalerShop | The updated WholesalerShop resource

try {
    $result = $apiInstance->patchWholesalerShopItem($id, $wholesaler_shop);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WholesalerShopApi->patchWholesalerShopItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **wholesaler_shop** | [**\OpenAPI\Client\Model\WholesalerShop**](../Model/WholesalerShop.md)| The updated WholesalerShop resource | [optional]

### Return type

[**\OpenAPI\Client\Model\WholesalerShop**](../Model/WholesalerShop.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/merge-patch+json
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## postWholesalerShopCollection

> \OpenAPI\Client\Model\WholesalerShop postWholesalerShopCollection($wholesaler_shop)

Creates a WholesalerShop resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\WholesalerShopApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$wholesaler_shop = new \OpenAPI\Client\Model\WholesalerShop(); // \OpenAPI\Client\Model\WholesalerShop | The new WholesalerShop resource

try {
    $result = $apiInstance->postWholesalerShopCollection($wholesaler_shop);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WholesalerShopApi->postWholesalerShopCollection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wholesaler_shop** | [**\OpenAPI\Client\Model\WholesalerShop**](../Model/WholesalerShop.md)| The new WholesalerShop resource | [optional]

### Return type

[**\OpenAPI\Client\Model\WholesalerShop**](../Model/WholesalerShop.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/ld+json, application/json, text/html
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## putWholesalerShopItem

> \OpenAPI\Client\Model\WholesalerShop putWholesalerShopItem($id, $wholesaler_shop)

Replaces the WholesalerShop resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\WholesalerShopApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 
$wholesaler_shop = new \OpenAPI\Client\Model\WholesalerShop(); // \OpenAPI\Client\Model\WholesalerShop | The updated WholesalerShop resource

try {
    $result = $apiInstance->putWholesalerShopItem($id, $wholesaler_shop);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WholesalerShopApi->putWholesalerShopItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **wholesaler_shop** | [**\OpenAPI\Client\Model\WholesalerShop**](../Model/WholesalerShop.md)| The updated WholesalerShop resource | [optional]

### Return type

[**\OpenAPI\Client\Model\WholesalerShop**](../Model/WholesalerShop.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/ld+json, application/json, text/html
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

