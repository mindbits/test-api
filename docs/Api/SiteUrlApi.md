# OpenAPI\Client\SiteUrlApi

All URIs are relative to *https://ve-s1.jamm.media*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteSiteUrlItem**](SiteUrlApi.md#deleteSiteUrlItem) | **DELETE** /api/site_urls/{id} | Removes the SiteUrl resource.
[**getSiteUrlCollection**](SiteUrlApi.md#getSiteUrlCollection) | **GET** /api/site_urls | Retrieves the collection of SiteUrl resources.
[**getSiteUrlItem**](SiteUrlApi.md#getSiteUrlItem) | **GET** /api/site_urls/{id} | Retrieves a SiteUrl resource.
[**patchSiteUrlItem**](SiteUrlApi.md#patchSiteUrlItem) | **PATCH** /api/site_urls/{id} | Updates the SiteUrl resource.
[**postSiteUrlCollection**](SiteUrlApi.md#postSiteUrlCollection) | **POST** /api/site_urls | Creates a SiteUrl resource.
[**putSiteUrlItem**](SiteUrlApi.md#putSiteUrlItem) | **PUT** /api/site_urls/{id} | Replaces the SiteUrl resource.



## deleteSiteUrlItem

> deleteSiteUrlItem($id)

Removes the SiteUrl resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SiteUrlApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 

try {
    $apiInstance->deleteSiteUrlItem($id);
} catch (Exception $e) {
    echo 'Exception when calling SiteUrlApi->deleteSiteUrlItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getSiteUrlCollection

> \OpenAPI\Client\Model\SiteUrl[] getSiteUrlCollection($page)

Retrieves the collection of SiteUrl resources.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SiteUrlApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$page = 56; // int | The collection page number

try {
    $result = $apiInstance->getSiteUrlCollection($page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SiteUrlApi->getSiteUrlCollection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| The collection page number | [optional]

### Return type

[**\OpenAPI\Client\Model\SiteUrl[]**](../Model/SiteUrl.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getSiteUrlItem

> \OpenAPI\Client\Model\SiteUrl getSiteUrlItem($id)

Retrieves a SiteUrl resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SiteUrlApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 

try {
    $result = $apiInstance->getSiteUrlItem($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SiteUrlApi->getSiteUrlItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\OpenAPI\Client\Model\SiteUrl**](../Model/SiteUrl.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchSiteUrlItem

> \OpenAPI\Client\Model\SiteUrl patchSiteUrlItem($id, $site_url)

Updates the SiteUrl resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SiteUrlApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 
$site_url = new \OpenAPI\Client\Model\SiteUrl(); // \OpenAPI\Client\Model\SiteUrl | The updated SiteUrl resource

try {
    $result = $apiInstance->patchSiteUrlItem($id, $site_url);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SiteUrlApi->patchSiteUrlItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **site_url** | [**\OpenAPI\Client\Model\SiteUrl**](../Model/SiteUrl.md)| The updated SiteUrl resource | [optional]

### Return type

[**\OpenAPI\Client\Model\SiteUrl**](../Model/SiteUrl.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/merge-patch+json
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## postSiteUrlCollection

> \OpenAPI\Client\Model\SiteUrl postSiteUrlCollection($site_url)

Creates a SiteUrl resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SiteUrlApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$site_url = new \OpenAPI\Client\Model\SiteUrl(); // \OpenAPI\Client\Model\SiteUrl | The new SiteUrl resource

try {
    $result = $apiInstance->postSiteUrlCollection($site_url);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SiteUrlApi->postSiteUrlCollection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **site_url** | [**\OpenAPI\Client\Model\SiteUrl**](../Model/SiteUrl.md)| The new SiteUrl resource | [optional]

### Return type

[**\OpenAPI\Client\Model\SiteUrl**](../Model/SiteUrl.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/ld+json, application/json, text/html
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## putSiteUrlItem

> \OpenAPI\Client\Model\SiteUrl putSiteUrlItem($id, $site_url)

Replaces the SiteUrl resource.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new OpenAPI\Client\Api\SiteUrlApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 'id_example'; // string | 
$site_url = new \OpenAPI\Client\Model\SiteUrl(); // \OpenAPI\Client\Model\SiteUrl | The updated SiteUrl resource

try {
    $result = $apiInstance->putSiteUrlItem($id, $site_url);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SiteUrlApi->putSiteUrlItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |
 **site_url** | [**\OpenAPI\Client\Model\SiteUrl**](../Model/SiteUrl.md)| The updated SiteUrl resource | [optional]

### Return type

[**\OpenAPI\Client\Model\SiteUrl**](../Model/SiteUrl.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/ld+json, application/json, text/html
- **Accept**: application/ld+json, application/json, text/html

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

