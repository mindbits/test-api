<?php
/**
 * SiteUrlApiTest
 * PHP version 5
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * 
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.2.2
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace OpenAPI\Client;

use \OpenAPI\Client\Configuration;
use \OpenAPI\Client\ApiException;
use \OpenAPI\Client\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * SiteUrlApiTest Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class SiteUrlApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for deleteSiteUrlItem
     *
     * Removes the SiteUrl resource..
     *
     */
    public function testDeleteSiteUrlItem()
    {
    }

    /**
     * Test case for getSiteUrlCollection
     *
     * Retrieves the collection of SiteUrl resources..
     *
     */
    public function testGetSiteUrlCollection()
    {
    }

    /**
     * Test case for getSiteUrlItem
     *
     * Retrieves a SiteUrl resource..
     *
     */
    public function testGetSiteUrlItem()
    {
    }

    /**
     * Test case for patchSiteUrlItem
     *
     * Updates the SiteUrl resource..
     *
     */
    public function testPatchSiteUrlItem()
    {
    }

    /**
     * Test case for postSiteUrlCollection
     *
     * Creates a SiteUrl resource..
     *
     */
    public function testPostSiteUrlCollection()
    {
    }

    /**
     * Test case for putSiteUrlItem
     *
     * Replaces the SiteUrl resource..
     *
     */
    public function testPutSiteUrlItem()
    {
    }
}
